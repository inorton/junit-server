junit-server
============

Junit-Server is a self-hosted server for long-term storage and display of test reports.

The first version is intended to support Junit format but will follow the test report formats understood by junit2html
